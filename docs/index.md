Salut ! Bienvenue sur ma page. Je vous laisse parcourir l' avancement de mon travail de Design. 

## A propos de moi

![](images/fatimaprofil.JPG) 

Je m’appelle Fatima Banga.Je suis étudiante en dernière année en architecture. Depuis petite, je bricole, je peins, je dessine ( quelques travaux publiés sur instagram (<a href="https://www.instagram.com/fatoum.hisbis/" > lien</a>). Je suis une adepte du fait maison et du recyclage. Je pars de l’idée que tout (ou presque) peut être fabriqué chez soi. Je porte un intérêt pour la création et le travail manuel. Je viens d’une famille où faire avec ses mains est naturel, si j’ai besoin de quelque chose, j’essayais d’abord de la faire moi-même. J'adore cuisiner. J'aime développer ma créativité à travers de nouveaux plats et bricolages. Je suis également une grande fan du cinéma iranien et égyptien. 

## Mon parcours

Née à Baghdad, j'ai parcouru quelques pays avec mes parents avant d'arriver en Belgique à l'âge de six ans. J'ai grandi près de Schuman. J'ai passé beaucoup de temps aux Ateliers du Soleil, une école de devoir qui m'a appris le partage, la collectivité , le recyclage, la poterie, etc. J'ai fais mes humanités à Adolphe Max en section scientifique. Tout cela m'a mené à vouloir étudier l'architecture. 

## Dernier projet

En 2019, j'ai eu l'occasion de travailler en équipe sur un jeu pour enfant fabriqué et pensé de manière à pouvoir être reproduit à la main et également en impression 3D sur base de récupération de fibres végétales (bagasse). Je vous invite à consulter  le projet pour plus d'informations. (<a href="https://fablab-ulb.gitlab.io/enseignements/2019-2020/fablab-studio/website/final_projects/jeu-artisanat-enseignement/tutorial/" > lien</a>) 


## Choix de l'objet

![](images/wekemusee.jpg)

Photographie par Fatima Banga 

Après avoir fait un tour au musée ADAM - Brussels Design Museum - et avoir regardé quasi toute la totalité de la collection exposée, mon choix  s'est porté sur le sac Weke conçu par Sarah Vigueur, Evelyne  Ouedraogo & co. Ce qui m'a plu dans cet objet c'est ton authenticité et l'histoire qu'il laisse paraître. En le regardant de plus près, j'ai commencé à décortiquer tout le procéssus de fabrication de l'objet sans même trop le connaître. Ce qui m'a attiré dans ce sac c'est le fait qu'il soit conçu à partir de déchets recyclés : pneu usagé . 
