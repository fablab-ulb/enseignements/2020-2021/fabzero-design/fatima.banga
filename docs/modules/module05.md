# 5. Formation Shaper Origin

|Mardi 17 Novembre 2020
| -:|

Aujourd'hui j'ai suivi une formation pour apprendre à utiliser la Shaper Origin avec Axel Cornu. 
La formation s'est fait en deux temps ( théorie puis pratique). 

![](../images/shaper.jpg) 

- Le Shaper est une CNC portative, à usage manuelle( ou une fraiseuse manuelle numérique). Il fonctionne un peu comme une CNC traditionnelle, avec des axes X, Y et Z, mais comme il est portable, sans table ni portique. ll est peu encombrant. 

- L'avantage est qu'on peut travailler sur une **surface de découpe infinie**. La machine permet également de faire des **réglages in situ**, c'est-à-dire qu'on peut à tout moment décider de faire des changements dans la découpe et de corriger des erreurs. 

- La **profondeur de découpe** ne peut dépasser **43 mm** . 

- Le **diamètre** du **collet** : **8 mm ou 1/8"**

- Le **port USB** permet un transfert hors ligne facile des fichiers de conception. Fonctionne avec les fichiers vectoriels standard ( **format .SVG**)

- La CNC possède un **écran** qui permet de suivre le chemin du fraisage à faire. 

**Il faut suivre quelques étapes avant d'utiliser l'outil :**

1. Travailler sur un **support stable** ( utiliser des serres joints ou des vis pour stabiliser le plan de travail). 
2. Déposer le planche qu'on décide de découper sur le plan de travail. La planche doit être **scotchée sur le plan de travail** ( utiliser un scotch double face ). Faire attention à la matière avec laquelle on souhaite travailler, pour ne pas abimer la fraise. 
3. Utiliser le **ShaperTape** pour permettre l'**identification** de la  **zone de travail**. Shaper Origin détecte et stocke automatiquement l'espace de travail. Il ne faut pas en placer beaucoup côte à côte, mais un peu dans tous les sens de façon à ce que la machine puisse scanné et identifier la planche. Il faut découper au minimum 3 les dominos dessinés sur le ShaperTape sinon la machine ne pourra pas scanner correctement. 
4. Il faut ensuite appuyer sur l'option " **scan** " situé sur l'écran de commande. La machine scanne la surface de travail grâce à la caméra. Il faut juste déplacer quelques fois la machine pour scanner toute la surface et que la machine fasse la cartographie de la surface de travail. 
5. Placer une **fraise adéquate** pour la découpe et la profondeur de découpe ( suivre les étapes de changements de fraise plus bas).
6. Selectionner le **dessin** sur l'écran de commande ( sous clé usb - format .SVG) ou en effectuer  un dessin hors ligne. Une fois le dessin selectionné, il faut placer le dessin là où on désire grâce à l'écran. Une fois l'emplacement trouvé, il faut appuyer sur le bouton vert situé sur la manette droite de la machine. 
7. Il faut absolument indiquer la **hauteur** en selectionné l'option **Z TOUCH** sur l'écran. La fraise va descendre automatiquement jusqu'a ce qu'elle toucher la surface de travail. Elle définie la hauteur. 

La découpe peut alors débuter. 

8. Maiiis, il faut d'abord placer le câble d'alimentation sur la fraise, puis brancher l'aspirateur dans la sortie adaptée sur le Shaper, puis allumer l'aspirateur.  C'est important de le faire !! Il faut ensuite placer le petit écran transparent aimanté sur la partie inférieur autour de la fraise pour ne pas avoir de la siure partout. On peut alors commencer la découpe en appuyant sur ON de l'interrupteur placé sur la fraiseuse. Pour arrêter le fraisage, il faut appuyer sur OFF. 

9. On peut passer sur le mode CUT. L'écran du Shaper indique le chemin à suivre pour déplacer la machine. Il indique la direction à prendre automatiquement. Il n'est pas possible de prendre la direction inverse à celle indiquée par l'écran. Si on dévie du chemin, la fraiseuse s'arrête et la fraise remonte au dessus de la découpe en cours. Il faut alors revenir sur la découpe déjà effectuée et appuyer sur le bouton vert de la manette droite. Le fraisage continue. 

Une fois la première profondeur de découpe effectuée, il faut indiquer sur l'écran de commande la profondeur suivante qu'on souhaite faire. 


**Changement de fraise :**

1. On arrête la fraiseuse ( OFF ). On enlève le câble d'alimentation. 
2. Devisser la fraiseuse avec la clé contenue dans la boîte et et faire coulisser délicatement la fraiseuse vers le haut pour la sortir. 
3. Appuyer sur le bouton de blocage. Changer la fraise. 
4. Refixer et visser  le tout avec une clé tout en appuyant toujours le bouton de blocage. 
5. Replacer la fraiseuse au dessus de son emplacement et la faire coulisser vers le bas.
6. Revisser la fraiseuse avec la clé. 
7. Ztouch etc. 


**Quelques photos de la formation :**

|![](../images/formationshaper1.jpg)|![](../images/formationshaper2.jpg) | ![](../images/formationshaper3.jpg)|
| -:| -:| -:|

|![](../images/formationshaper4.jpg)|![](../images/formationshaper5.jpg) | ![](../images/formationshaper6.jpg)|
| -:| -:| -:|

