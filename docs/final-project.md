# PROJET FINAL - WEABAG

|![weabag](images/protofinal1.jpg)|![weabag](images/protofinal3.jpg)|![weabag](images/protofinal2.jpg)|
| -:| -:| -:|

L’objet final, le Weabag, est un sac qui peut être utilisé de différentes manières : 
- fermé, l’objet devient un sac de course ou un sac à dos; 
- ouvert, il se transforme en tapis sur lequel on pourrait s’asseoir à l’extérieur pour pique-niquer.

La démarche de la création du sac s’inspire de celle qui a été employé par Sarah Viguer : utilisation de matériaux récupérés, et des savoirs-faire et outils de fabrications locaux.
La conception de sac reste simple et il peut être fabriqué par quiconque au sein d’un FabLab à l’aide d’une découpeuse laser.

Le sac Weabag peut être fabriqué avec n’importe quels matériaux, permettant à chacun de le customiser selon ses goûts.

|![pdf-affiche](docs/Fichiers/a4_fatimaa.pdf)

## Un sac, plusieurs possibilités

| ![ferme](images/finalweabag1.png)|![ouvert](images/finalweabag2.png)|
| -:| -:|

## Coûts et durée de conception

Tous les prototypes ont été réalisé à partir de matériaux récupérés. 

Mais voici le prix des matériaux neufs : 

- Tissus : 4 à 10 euros le m2 de tissus neufs en moyenne ( en fonction de la qualité du tissu)
- Cordon : 5 euros
- Planche de bois multiplex 5mm ( dimension 120x60) : 7,50 euros

        Total = +/- 20 euro

- Découpe laser : 20 minutes 
- Temps d'assemblage : 1 heure 


## Fichiers

- [Découpe1 tissus externe](Fichiers/decoupe1_weabag.dxf)

- [Découpe2 tissus interne](Fichiers/decoupe2_weabag.dxf)

- [Découpe3 support rigide intérieur- bois ](Fichiers/decoupe_bois_weabag.dxf)

## Réalisation 

### Dimensions

![dimension](images/dimension.jpg)
Si vous voulez reproduire le sac Weabag, il vous faut : 


1. 4m2 de tissus acheté ou récupéré 
2. Une planche en bois de 5mm d'épaisseur 
3. Un lacet/ un cordon/ un fil tressé que vous pouvez concevoir à partir de plusieurs fils épais de 6m de long et 8mm d'épaisseur

Ensuite 

4. Télécharger les fichiers du Weabag
5. Une découpeuse laser (sinon référez-vous au prototype artisanal) 
        Réglage utilisé : puissance 50, rapidité 35% (tout dépend du textile)



### Assemblage

![realisationWeabag](images/weabag_realisation.gif)

6. Passer les fils à travers les emplacements / trous respectifs. 
7. Une fois les deux tissus intérieur et extérieur assemblés, passer le cordon selon le besoin  ( sac à dos, sac bandouillère, tapis) à l'intérieur des grands trous tout autour de la bordure. 
8. Une fois que tout est assembler, il vous suffit de tirer que le cordon pour fermer le sac et de relacher la tension du cordon afin d'ouvrir le sac


## Méthodologie de travail 

Mes recherches sont divisés en 3 parties :

1. Décomposition : je décompose l'objet de référence afin de mieux comprendre sa conception, sa réalisation, et les choix des matériaux. 
2. Analyse : Une fois l'objectif principal déterminé, je m'inspire de la méthodologie de conception afin d'analyser au mieux quels choix de matériaux opter pour les prototypes. Débutes une phase expérimentale avec différents matériaux, différentes techniques d'assemblage afin de répondre à l'objectif principal, qui est de créer un sac dans le contexte actuel dans lequel je me trouve. 
3. Recomposition : Phase de prototypages de différents sac grâce aux éléments que j'ai sélectionné dans la phase d'analyse. 

# RECHERCHES

# Journal de Bord

|Jeudi 01 Octobre 2020
| -:|


Premier jour au cours d'option Design, on a commencé la matinée par regarder une vidéo de 20 minutes qui retrace toute l'histoire du fameux BIC que tout le monde connait. J'ai trouvé cette vidéo très intéressante et me rappela les vidéos de l'émission " comment c'est fait " que je regardais quand j'étais ado. Ce qui est beau, c'est qu'au final chaque objet qu'on trouve autour de soi à une vraie histoire à raconter, il suffit juste de s'y intéresser. Autre chose que j'ai trouvé chouette, c'est que j'ai l'impression que ce quadri en option design va me permettre de complèter mon quadri passé au sein de l'atelier DFS en 2019-2020 et de plus intégrer cette mentalité " cubaine " à vouloir tout comprendre d'un objet et surtout de ne pas se limiter à se servir de l'objet d'une seule façon, mais de voir d'autres opportunités d'utilisations pour chaque composante de l'objet. 

L'après midi, une petite visite au Adam Design Museum, et choix de l'objet sur lequel on va travailler dessus durant tout le quadri. 
( descendre plus loin sur la page pour plus d'infos)


## Choix de l'objet

|Jeudi 22 octobre 2020 
| -:|

![weke](images/sacwekebis.jpg)
sac Weke conçu par Sarah Vigueur, Evelyne  Ouedraogo & co.

2013, Ouagadougou - BU

En collaboration avec Hors Pistes Project. 

Photographie par Sarah Vigueur 

## Observations et description


| ![sketch](images/0001_1_.jpg) |![sketch](images/0002_1_.jpg) | ![sketch](images/sketch3.jpg) |
| -:| -:| -:|

( Dessins par FB )

Après avoir fait un tour au musée ADAM - Brussels Design Museum - et avoir regardé quasi toute la totalité de la collection exposée, mon choix  s'est porté sur le sac Weke conçu par Sarah Vigueur, Evelyne  Ouedraogo & co. Ce qui m'a plu dans cet objet c'est ton authenticité et l'histoire qu'il laisse paraître. En le regardant de plus près, j'ai commencé à décortiquer tout le procéssus de fabrication de l'objet sans même trop le connaître. Ce qui m'a attiré dans ce sac c'est le fait qu'il soit conçu à partir de déchets recyclés : **pneu usagé** .
Le sac Weke est composé de différentes matières : **caoutchouc, fil de coton**. Il a l'air d'un sac **léger et compact** qui peut être défait et enroulé sur lui même. Ce qui est intéressant c'est que le fil de pneu offre une **rigidité** à la structure du tissage. Le fil de caoutchouc permet également de donner une **forme au tissage**. Le **fil de caoutchouc** dépasse légèrement de la trame du tissage, ce qui permet de l'utiliser comme **accroche**. 

## Comprendre la conception du sac Weke

| Jeudi 29 octobre 2020 
| -:| 

## Designer 

![sarahviguer](images/sarahviguerprofil.jpg) <br>( Photographie MDM 2018 )</br>

Sarah Viguer est une designer française ( Marseille). Ses recherches portent sur les **textiles**, **l'artisanat local**, les **matériaux éco-conçus** et les méthodes de projets à partir de **mélanges culturelles** et **disciplinaires**. Elle est fondatrice de **Xufa Proceso** (une initiative de design basée à Valence, avec l'objectif d'explorer les déchets écologiques issus de l'agriculture des racines de xufa. 

## Contexte de la création 

### Milieu social

- **Workshop en résidence organisé par  Hors Piste Project**

Sarah Vigeur participa en 2013 à un  workshop organisé par **Hors Pistes Project** qui s'est déroulé à Ouagadougou, Burkina Faso. A travers ces workshops,  les jeunes designers découvrent les techniques artisanales des artisans, et les artisans des visions nouvelles des designers venus d'ailleus. Tous les deux diversifient leurs champs de savoir-faire. Cette collaboration permet également de questionner l'héritage des techniques artisanales et leurs destins dans notre monde industrialisé. 

- **Objectifs visés par Hors Pistes Project**

1. Valoriser le savoir faire des artisans

2. Organiser des rencontres entre designer étrangers et artisans locaux

3. Organiser des ateliers. Chaque couple de designers-artisans travaille dans une organisation artisanale locale sur un matériau spécifique pendant un mois. Chaque designer crée un produit design conçu avec l'aide de l'artisan.

4. Organiser des expositions à partir des objets design produit par les artisans et designers. 

### Contexte social

- **Collaboration local et prémisses de la création**

![sarahviguer](images/conceptionweke.JPG) (photographie par Fabrice Schneider  - [vidéo](https://www.youtube.com/watch?v=JgGxi3Eoh_o&feature=emb_title&ab_channel=HorsPistesProject)
<br>Le sac Weke est le résultat d'une collaboration avec Maurice Nagalo, Evelyne Ouedraogo & Bibata (tisseuses) et Sarah Viguer.  Maurice Nagalo imagina le premier sac recyclé à partir de ces sacs en plastiques ramassés par **les femmes de la Brigade Verte de Ouagadoudou** qui avaient pour objectif de récollecter des sacs en plastiques usagés à des fins de recyclage. Sarah Vigueur, en déplacement dans la ville, renda visite aux artisans qui fabriquaient des objets à partir de pneus recyclés. Ces hommes coupaient de grandes chambres à air en fines bandettes régulières, qui allaient servir au futur projet du sac Weke. Intéressée par les fines bandelettes, Sarah Viguer et Evelyne Ouedraogo combinèrent leurs savoirs et leurs espaces pour concevoir le sac Weke. 

### Matériaux qui ont permis la création du sac Weke 

| Sacs en plastique ramassé par la Brigade Verte | Bobine de sac en plastique | Pneu coupé en languette | Chambre à air découpé en fine bandelette |
| --- | --- | --- | --- | 
| ![weke](images/reclyclagesacplastique.jpg) | ![weke](images/sacplastique2.jpg) |![weke](images/pneucouper.jpg) | ![weke](images/bandepneu.jpg) |

| Fil de coton teinté | Tissage avec la bandelette de caoutchouc | Tissage Weke | Sac Weke |
| --- | --- |  --- | --- | 
| ![weke](images/filbleu.jpg) | ![weke](images/pneuu.jpg) |![weke](images/wekee.jpg) | ![weke](images/wekesac.jpg) |

| Bandelettes caoutchouc | Métier à Tisser | Fil de coton |
| --- | --- | --- | 
|![weke](images/pneu.jpg)| ![weke](images/metieratisser.jpg) | ![weke](images/filcoton.jpg) |

( Photographies par Fabrice Schneider,2013,Ouagadougou)

<strong> Tissage avec le métier à tisser d'Evelyne Ouedraogo </strong>

![weke](images/tissageavecpneu.jpg)

( Photographie par Fabrice Schneider,2013,Ouagadougou)


## Relation Artisan - Designer 

|Jeudi 12 Novembre 2020
| -:|

La plupart des initiatives en lien avec le design se rencontrent dans les domaines de l’artisanat d’art où les collaborations entre artisans et designers sont devenues une pratique courante. Cependant, le design ne se contente pas de donner forme au produit.  Il implique tout un processus allant de la conception à la mise sur le marché, en passant par la fabrication, qui permet à l’entreprise de s’interroger sur sa capacité à innover, à répondre aux nouveaux besoins des consommateurs et à progresser dans la qualité.
Il pourrait exister une sorte d'effet barrières entre le designer et l'artisan, une difficulté d'un dialogue entre le détenteur d'un savoir-faire (artisan) et le détenteur d'un savoir et de connaissances (designer).


- **Contact avec Sarah Viguer**

![sv](images/contactsarahviguer.JPG) 

J'ai pris l'initiative de contacter Sarah Viguer pour en savoir plus sur  le projet du Sac Weke et de sa collaboration avec les différents artisans qui ont participé sur le projet.  ( à suivre)

- **Design Intuitif**

Ce qui intéressant, c'est de mettre la main à la pâte, toucher et percer le matériel dans tous les sens. Essayer de jouer à un jeu de création intuitif où les mains opèrent sans réflexion sur l'environnement, sans conceptualisation en amont, sans imaginer un scénario. Essayer, expérimenter les possibilités d'un design qui s'adapte à son environnement. 

## Méthodologie de création et de recherche : Hand Picking, geste inaugural pour tout projet 

1. Attraper de la matière première à portée de main et l'explorer dans tous les sens pour le sortir de son environnement immédiat.

2. Essayer de multitude d'échantillons jusqu'à tomber sur une surprise. Collecte d'une bibliothèque d'échantillons.

3. Créations de divers projets à base des test échantillons. 

### Recyclage du plastic 

( à suivre)

### Recycling vs Upcycling

| Dimanche 15 Novembre 2020
| -:|

Aujourd'hui, je me suis intéressée à deux mots qui se ressemblent de près mais qui ont une signification différente. **Recycling** & **Upcycling**

Le terme upcycling est souvent opposé au recycling / recyclage lorsqu'on parle de design. 
On pourrait dire que l' "upcycling est le nouveau recyclage".  
L'upcycling est la réutilisation d'un objet, d'un produit qui à la base ne sert plus, pour créer des objets plus qualitative. Donc pour résumer, upcyclé c'est : redonner de la valeur à un objet inutilisés. 
Alors que le recyclage implique de réutiliser des composants d'un produit, qui après avoir été liquéfiés, broyés, ils sont réduits en matière première, dans le but de l'utiliser pour créer un nouvel objet. 
La conversion d'un objet en matière première ( processus de recyclage ) nécessite une quantité d'energie considérable. 

L'upcycling et le recyclage sont deux techniques bien différentes ayant un but commun : la revalorisation de la matière. Mais le recyclage ne remplacera jamais l'upcycling ( vice versa ). 
L'upcycling est plus écologique que le recyclage mais tous les deux ont des limites. 

Le terme upcycling a été inventé il y a fort longtemps. Tout le monde a déjà fait l'expérience d'upcycling d'un objet. 
Certains designers du XXe siècle ont conçu des projets basés sur le principe de remploi des objets; comme **Enzo Mari**, en **1992** avec le kit **ECOLO** produit pour l'Atelier Alessi. Il suggère de manière provoquante le potentiel du " do-it-yourself ". Il écrit un livret dans lequel il met par écrit toutes les étapes pour transformer des bouteilles de détergent en vase décorative. Il sortie une série de bouteilles en vase signé. D'une certaine façon, il met en évidence l'importance du recyclage/ upcycling et  le statut/ rôle du design.  ( source : Renny Ramakers, Less + More: Droog Design in context, 010, Rotterdam, 2002, p. 130.)

![ECOLO](images/ecolo_enzo_mari.jpg) ![ECOLO](images/enzomari.jpg)

"ECOLO"

par Enzo Mari

pour Atelier Alessi

(1992) 1995

## Weaving code, Amandine David

| Mercredi 18 Novembre 2020
| -:|

![](images/weavingcode7.jpg)


Amandine David ( basée à Bruxelles) construit les ponts entre la fabrication numérique et l'artisanat traditionnel avec le tissage. Elle explore les motifs de tissages de divers cultures afin de connecter le tissage à la main à l'impression 3D. Le projet Weaving Code est conçu avec trois techniques artisanales : le tissage à la main, la programmation et l'impression 3D. Alors qu'un tissage est en cours, un objet installé sur le métier à tisser connecté lui même à un ordinateur, traduit le code binaire ( 0 et 1) sur le métier à tisser. Le numérique et le traditionnel se sont pas opposés, ils se valorisent l'un l'autre dans ce projet. 

![](images/weavingcode8.jpg)

|![](images/weavingcode1.JPG)|![](images/weavingcode2.JPG)|![](images/weavingcode3.JPG)|
| -:| -:| -:| 

|![](images/weavingcode4.JPG)|![](images/weavingcode6.jpg)|
| -:| -:| 

## Métier à tisser DOTI 

| Jeudi 19 Novembre 2020
| -:|

Développé par Pamela Liou, le métier à tisser DOTI est un métier à tisser jacquard de bureau open source. Il qui tire parti de la fabrication numérique pour permettre une production textile expressive à la maison et encourager une culture plus large du design. Le tisserand glisse et déposent une image, qui est ensuite analysée en un motif tissé. Un ensemble de moteurs soulève et abaisse les fils pendant que le tisserand fait passer une navette à travers la remise du métier à tisser, générant des motifs de tissu complexes. [lien](https://pamelaliou.com/blog/?p=418)

|![](images/Doti_slide1.png) |![](images/Doti_slide2.png) |![](images/Doti_slide3.png) |![](images/Doti_slide4.png)|
| -:| -:| -:| -:|

|![](docs/images/Doti_slide8.jpg)|
| -:| 


## Réinterprétation de l'objet

| Mardi 03 Novembre 2020
| -:|

Afin de mieux comprendre le sac Weke, j'ai essayé de construire un métier à tisser à partir de matériaux que j'ai trouvé chez moi pour mieux m'approprier l'objet. 

![tissage1](images/metiertisser5.jpeg)

|![](images/1.jpeg) |![](images/2.jpeg) |![](images/3.jpeg) |![](images/4.jpeg) |
| -:| -:| -:| -:|

|![](images/5.jpeg) |![](images/6.jpeg) |![](images/7.jpeg) |![](images/8.jpeg) |
| -:| -:| -:| -:|

- **Croquis / Recherche**

J'ai essayé d'imaginer d'autres utilisations du "tissage Weke". 
![sketch4](docs/images/sketch4.jpg)

| Mercredi 18 Novembre 2020
| -:|

![tissage2](images/tissage2_1.jpg) ![tissage2](images/tissage2_2.jpg) ![tissage2](images/tissage2_4.jpg)

| Samedi 20 Novembre 2020
| -:|

**Objet choisi ( sac Weke):** 

| Matière| Fonction| Technique|
|:- | --- | --- |
|- Caoutchouc <br> - Fil de coton <br> - Fil en plastique recyclé | - Sac <br> - Ravier <br> - Vêtement | - Tissage|

**Object réinterprétation:** 

| Matière| Fonction| Technique|
|:- | --- | --- |
|- Chambre à air/caoutchouc <br> - Fil de bouteille en plastique <br> - Fil  <br> - ABS/PLA <br> - bois <br> - Carton/papier maché | - Mobilier <br> - Accessoire | - Tissage <br> - Impression 3D <br> - Moulage ? |

| Lundi 23 Novembre 2020
| -:|

J'ai essayé de jouer mon rôle du designer en identifiant un problème pour imaginer une solution. Le problème avec l'objet que j'ai choisi, sac Weke, c'est qu'il a été crée dans un **contexte**, milieu, et environnement social et géographique **différent** de celui dans lequel je me trouve. Je dois donc continuer l'excercice de réinterprétation du nouvel objet avec le contexte dans lequel je me situe. **Rapporter l'objet en Belgique** signifie qu'il faut **identifier ce que je veux garder**: <br> - soit la méthodologie avec lequel l'objet a été crée; <br> - soit la fonction de l'objet choisi; <br> - soit l'approche. 

### **Imaginer un projet dans le contexte actuel et local.** 

* *Où suis-je ?* 

A Bruxelles en semaine et à Tirlemont le weekend. A Tirlemont, j'ai de la chance d'avoir beaucoup de matériaux de récupérations. A Bruxelles, je dois chercher des matériaux chez des particuliers, et j'ai à disposition les machines du fablab. 

* *Confinement...* 

Difficile de contacter et de collaborer avec des artisans/designers en ce moment. Il faudra trouver une autre solution...

* *Que vais-je faire ?* 

Jouer un double rôle : être artisan et designer en même temps. 

* *Que vais-je rapporter ? Quelle est la spécificité de mon approche ?* 

Peut-être : <br> - recherche de solutions pratiques, efficaces, séduisantes ? <br> - une facilité d'utilisation et de fabrication ? <br> - une approche DIY en utilisant les technologies du fablab et partageable avec le monde ? <br> **- un peu de tout à la fois** ? 

**TRANSMISSION** - **DIFFUSION** - **ACCESSIBILITE**

### **Design d'aujourd'hui (selon moi)**

-Discipline centrée sur la technologie vers une discipline tournée vers l'humain. <br> - Exercer ses compétences sur les problèmes qui comptent vraiment.<br> - Moins gadget <br> - Mieux vivre = s'adapter avec les contextes socio,politique,culturels actuels. <br> - Vers une certaine frugalité? Réduire le nombre d'objet dans notre vie plutôt que d'en créer de nouveaux. <br> - Combiner plusieurs fonctions en un objet ? 

| Mercredi 25 Novembre 2020
| -:|

## **Esquisse 1**

* Cette première réinterprétation de l'objet porte de l'intérêt à différents éléments que j'ai trouvé intéressant sur le sac Weke : sa technique de réalisation (tissage), l'utilisation de caoutchouc (très résistant), multiusage. 

* L'idée est de proposer un objet qui pourrait être construit par son/sa futur(e) utilisateur/trice. L'utilisateur devient à son tour designer/artisan. 

* A partir des mêmes pièces (connecteurs et bâtons), l'utilisateur pourra construire différents meubles selon ses besoins. 

![esquisse](images/esquisse.jpg)

| ![esquisse](images/reinterpretation2.jpg) | ![esquisse](images/reinterpretation1.jpg) |
| -:| -:|

| ![esquisse](images/reinterpretation3.jpg) | ![esquisse](images/reinterpretation4.jpg) |
| -:| -:|

| ![esquisse](images/reinterpretation5.jpg) | ![esquisse](images/reinterpretation6.jpg) |
| -:| -:|

## **Test Prototype** 

### **Connecteur à trois raccords**

| ![prototype](images/prototype3.jpg) | ![prototype](images/prototype4.jpg) |
| -:| -:|

| ![prototype](images/prototype1.jpg) | ![prototype](images/prototype2.jpg) |
| -:| -:|

### **Connecteur à quatre raccords**

| ![prototype](images/prototypec.jpg) | ![prototype](images/prototyped.jpg) |
| -:| -:|

| ![prototype](images/prototypea.jpg) | ![prototype](images/prototypeb.jpg) |
| -:| -:|


| Jeudi 3 Décembre 2020
| -:|

## **Esquisse 2**

| ![esquisse2](images/tiss1.jpg) | ![esquisse2](images/tiss2.jpg)  | ![esquisse2](images/tiss.jpg)  |
| -:| -:| -:|

| ![esquisse2](images/tiss3.jpg) | ![esquisse2](images/tiss4.jpg)  | ![esquisse2](images/tiss5.jpg)  |
| -:| -:| -:|

| ![esquisse2](images/tiss6.jpg) | ![esquisse2](images/tiss7.jpg)  |
| -:| -:|

| ![esquisse2](images/tiss8.jpg) | ![esquisse2](images/tiss9.jpg)  |
| -:| -:|

| ![esquisse2](images/tiss14.jpg) | 
| -:|

| ![esquisse2](images/tiss10.jpg) | ![esquisse2](images/tiss11.jpg)  |
| -:| -:|

| ![esquisse2](images/tiss12.jpg) | ![esquisse2](images/tiss13.jpg)  |
| -:| -:|

| ![esquisse2](images/tiss15.jpg) | 
| -:|

| Jeudi 10 Décembre 2020
| -:|

## **Esquisse 3**

Suite aux différents prototypes que j'ai proposé jusqu'à présent, j'ai pris la décision de travailler sur un nouvel prototype dans lequel j'inclue l'idée d'**associer** le métier à tisser ( **la structure du métier à tisser**) à **l'objet** pour n'en faire qu'un. 

Pour cette troisième esquisse, j'ai décidé de travailler sur un prototype plus grand que ce j'ai fais jusqu'à présent. L'idée est de concevoir un objet qui permet **plusieurs fonctions** à la fois et deux tailles différentes : **taille S : sac et tapis pour s'asseoir** & **taille XL : sac et hamac**. 

### **Croquis/ Idées**

![esquisse3](images/esquisse3_schema.jpg)

### **Prototype** 

**Matériaux :**

Pour la structure : 

- Batons avec un système d'accroche

Pour le tissage : 

- Sacs en plastique ( fil horizontal)
- Chambre à air (fil horizontal)
- Fil de coton  (fil vertical)

**Etapes de réalisations (artisanal) :**

| ![esquisse3](images/esquisse3_8.jpg) | ![esquisse3](images/esquisse3_9.jpg)  | ![esquisse3](images/esquisse3_10.jpg) |
|---|---|---|
| **Etape 1**: construction de la structure  <br> Faire des indications à intervalle régulier sur le baton afin d'indiquer l'emplacement des futurs trous.  | **Etape 2**: A l'aide d'une perceuse électrique, faire les trous aux endroits indiqués plus tôt | **Etape 3**: Recommencer  l'étape 1 et 2 sur le second baton.|


| ![esquisse3](images/esquisse3_11.jpg) | ![esquisse3](images/esquisse3_13.jpg)  |
|---|---|
|**Etape 4** : Découper des fils de longeur d'1,5m. Ensuite faites les passer dans les trous situés sur les batons. Faire attention aux positions des fils sur les deux batons ( ne pas faire de croisement entre les fils)  |**Etape 5** : Une fois les fils verticaux mis en place, faire des noeuds de part et d'autres du fils afin de les fixer et qu'ils ne s'échappent pas des trous. Placer toute la structure de façon à ce que le travail de tissage soit facile et confortable. J'ai placé la structure entre deux planches de bois pour qu'elle ne bouge pas, à une hauteur de +/- 80 cm de haut. |

| ![esquisse3](images/esquisse3_14.jpg) | ![esquisse3](images/esquisse3_12.jpg) |
|---|---|
|**Etape 6** : Faire Passer les fils horizontaux une fois en dessous des fils verticaux, une fois au dessus des fils verticaux. Une fois la rangée terminée, serrer le fil horizontal vers le bas, à l'aide d'un peigne/doigts| **Etape 7** : Recommencer l'étape 6 plusieurs fois. Introduire une rangée de chambre à air à intervalle régulier. Une fois le tissage terminé, nouer les fils entre eux.|

| ![esquisse3](images/esquisse3_2.jpg) | ![esquisse3](images/esquisse3_3.jpg)  |
| -:| -:| 

| ![esquisse3](images/esquisse3_1.jpg) | ![esquisse3](images/esquisse3_7.jpg) |
| -:| -:|

| ![esquisse3](images/esquisse3_6.jpg) | ![esquisse3](images/esquisse3_15.jpg)  |
| -:| -:|



**La suite**

- Comment améliorer la structure ? Retravailler le métier à tisser en parallèle...
- Système d'accroche fabriqué à partir de découpe au laser 
- Comment tenir le sac ? Penser au poignée ? 
- Comment fermer les côtés ? Pour pouvoir l'utiliser comme un sac ...
- Introduire des éléments horizontaux imprimés en 3D ou coupés au laser à la place des chambres à air ? 
- Penser à une trame et les épaisseurs des fils + nombres de fils verticaux !!

![esquisse3](images/esquisse3_suite.jpg)

| Mercredi 16 Décembre 2020
| -:|

## **Esquisse 4 : Weabag 1**

Après avoir analysé et compris le sac Weke sous tous ses angles, j'ai identifié quelques problèmes liés à l'usage du sac weke : 
<br> - Il n'y a pas de poignée( il faut le porter à deux mains afin de le déplacer); 
<br>- Les côtés du sac ( plus ou moins fermé grâce au système d'emboitement du caoutchouc) ne permettent pas de contenir un objet.
Le prototype de l'esquisse 4 est un prototype d'essai composé de différents test effectué afin d'améliorer ma proposition de l'esquisse 3.
J'ai essayé d'intégrer une poignée afin de permettre de tenir le sac à une main et travailler sur les fermetures des côtés. 
L'objet est travaillé de manière à permettre à tout le monde de le reproduire chez soi, sans spécialement avoir un métier à tisser. 


### **Idées en vrac **

![esquisse4](images/esquisse4_18.jpg) 
|---|

|![esquisse4](images/esquisse4_19.jpg) |![esquisse4](images/esquisse4_20.jpg)|
|---|---|

![esquisse4](images/esquisse4_21.jpg) 

### **Prototype**

![esquisse4](images/esquisse4_15.jpg)


### **Etapes de réalisation et remarques**

| ![esquisse4](images/esquisse4_1.jpg) | ![esquisse4](images/esquisse4_2.jpg)  |
|---|---|
|Pour ce prototype, j'ai travaillé avec des fils verticaux de 1mm d'épaisseur pour pouvoir obtenir des petites mailles serrées. L'inconvénient avec ce fil d'1 mm c'est qu'il faut en nouer énormement pour atteindre les dimensions voulues.| Une fois les fils verticaux noués sur la structure, regrouper par paquet afin de faciliter le travail par la suite. |

| ![esquisse4](images/esquisse4_3.jpg) | ![esquisse4](images/esquisse4_4.jpg)  |
|---|---|
|Attacher les fils verticaux à la barre inférieure. Ne pas tendre très fort les fils. Laisser une marge de souplesse dans le fil vertical. | Tous les fils doivent être tendus et attachés à la même distance ( même entraxe )|

| ![esquisse4](images/esquisse4_5.jpg) | ![esquisse4](images/esquisse4_6.jpg)  |
|---|---|
|Cette astuce permet de placer plus facilement les fils horizontaux qui viendront par la suite. Il suffit de distinguer les fils A des fils B (un fil sur 2) et de glisser une latte en bois entre les fils. | A ce stade de la mise en place de la structure du tissage, il est encore possible d'effectuer des changements ou des rajouts de fils verticaux. |

| ![esquisse4](images/esquisse4_7.jpg) | ![esquisse4](images/esquisse4_8.jpg)  |
|---|---|
| Une fois tous les fils verticaux noués, il suffit de passer aux fils horizontaux. Le fil horizontal doit passer une fois au dessus, une fois en dessous des fils verticaux. | Afin de travailler proprement sur son tissage, il faut auparavant travailler sur le design du tissage (couleur, motif, texture ). Ici le design du tissage est très basique, composé de trois fils hozizontaux différents. Les fils jaune permettent de fermet le sac sur les côtés une fois le tissage terminé. |

| ![esquisse4](images/esquisse4_9.jpg) | ![esquisse4](images/esquisse4_10.jpg)  |
|---|---|
|J'ai également intégré des fils en caoutchouc afin de donner à l'objet de la rigidité et une structure portante.  | J'ai voulu intégré et exploité l'idée d'une poignée conçu avec un fil en caoutchouc (sans devoir ajouter ou greffer une poignée étrangère).|

| ![esquisse4](images/esquisse4_11.jpg) | ![esquisse4](images/esquisse4_12.jpg)  |
|---|---|
|J'ai testé de gonfler la chambre à air afin de voir si en le gonflant, ça ajouterait une plus value. L'idée est intéressante, mais peu pratique pour la fonction de tapis, une fois le sac déballé. | Le problème dans tous les autres prototypes faits jusqu'à présent c'est la fermeture des côtés du sac. L'idée est de créer un élément tubulaire permettant d'intégrer un lacet/ fil/ élastique à l'intérieur. Le principe de fermeture reste simple : il suffit de tirer sur le lacet pour fermer les côtés du sac. |

| ![esquisse4](images/esquisse4_13.jpg) | ![esquisse4](images/esquisse4_14.jpg)  |
|---|---|
| Fermeture des côtés du sac| Fermeture des côtés du sac|

| ![esquisse4](images/esquisse4_16.jpg) | ![esquisse4](images/esquisse4_17.jpg)  |
|---|---|


### **Croquis de l'objet imaginé**

![esquisse4](images/esquisse4_22.jpg) 

| Jeudi 7 Janvier 2021
| -:|

## **Esquisse 5 : Weabag 1.2**

Je continue mes recherches sur les matériaux adéquats pour tisser et j'ai une idée lorsque j'observe la cagette de mandarine qui se trouve dans la cuisine. Au dessus de la cagette se trouve un filet orange. Je me dis que je pourrais expérimenter un tissage à travers les mailles du filet. 

![esquisse5](images/caissemandarine.jpg) 

|![esquisse5](images/filetrouge2.jpg)| ![esquisse5](images/filetrouge1.jpg)|
|---|---|

Je trouve l'assemblage intéressant, et je continue dans mon élan avec du filet de pomme de terre et de bois. Je me demande ce que ça donnerait si je rajoute à la maille une structure. Je trouve que cette assemblage pourrait servir pour divers utilités : hamac, chaise , sac . 


|![esquisse5](images/hamac1.jpg)| ![esquisse5](images/hamac2.jpg)|![esquisse5](images/hamac3.jpg)|
|---|---|---|

|![esquisse5](images/hamac4.jpg)| ![esquisse5](images/hamac5.jpg)|![esquisse5](images/hamac6.jpg)|
|---|---|---|

Je continue mes explorations à travers cette fois ci un bout de tissu que je trouve chez moi. Je le trouve intéressant ; léger, élastique, composée d'une maille en cercle. Je le teste sans attendre à l'aide d'un fil et d'une aiguille. Je fais un tissage à travers un tissu en maille. Je me dis que cette idée de tissu en maille pourrait être une bonne idée pour assembler un sac. 

|![esquisse5](images/sacfilet1.jpg)| ![esquisse5](images/sacfilet2.jpg)|
|---|---|


|![esquisse5](images/sacfilet3.jpg)| ![esquisse5](images/sacfilet4.jpg)|![esquisse5](images/sacfilet5.jpg)| ![esquisse5](images/sacfilet6.jpg)|
|---|---|---|---|

## **Esquisse 6 : Weabag 1.3**

Après avoir discuté avec Victor, Gwendoline et Hélène, je prend la décision de travailler la structure maillée à travers un tissu. Je dessine le patron d'un sac sur autocad afin de pouvoir utiliser la découpeuse laser. Je lance la découpe et l'assemble avec différents fils. 
Ce qui est intéressant est d'apercevoir que finalement la structure du tissage se retrouve dans les mailles. 
Je dessine différentes formes sur le patron qui correspondent chacune à une fonction dans le tissage. Une maille est destinée à l'assemblage du sac, une autre maille est destinée aux poignées du sac. 

|![esquisse6](images/esquisse6_1.jpg)| ![esquisse6](images/esquisse6_2.jpg)|
|---|---|

|![esquisse6](images/esquisse6_3.jpg)| ![esquisse6](images/esquisse6_4.jpg)|
|---|---|

|![esquisse6](images/esquisse6_5.jpg)| ![esquisse6](images/esquisse6_6.jpg)|
|---|---|

## **Esquisse 7 : Weabag 2 - Prototype Artisanal**


|![esquisse7](images/esquisse7_1.jpg)| ![esquisse7](images/esquisse7_2.jpg)|![esquisse7](images/esquisse7_3.jpg)| ![esquisse7](images/esquisse7_4.jpg)|
|---|---|---|---|

|![esquisse7](images/esquisse7_5.jpg)| ![esquisse7](images/esquisse7_6.jpg)|![esquisse7](images/esquisse7_7.jpg)| ![esquisse7](images/esquisse7_8.jpg)|
|---|---|---|---|

|![esquisse7](images/esquisse7_88.jpg)| ![esquisse7](images/esquisse7_9.jpg)|![esquisse7](images/esquisse7_10.jpg)| ![esquisse7](images/esquisse9_11.jpg)|
|---|---|---|---|

|![esquisse7](images/esquisse9_12.jpg)| ![esquisse7](images/esquisse9_13.jpg)|![esquisse7](images/esquisse9_14.jpg)|
|---|---|---|




## **Esquisse 8 : Weabag 2 - Prototype Laser 1**

( Les prototypes que j'ai réalisé sont plus petits que prévu et coupés en deux morceaux car la grande découpeuse laser était en panne et j'ai du utilisé la petite découpeuse laser) 

![realisationWeabag](images/weabag_realisation.gif)

|![esquisse8](images/esquisse8_1.jpg)|![esquisse8](images/esquisse8_2.jpg)|
|---|---|

|![esquisse8](images/esquisse8_3.jpg)|![esquisse8](images/esquisse8_4.jpg)|
|---|---|

## **Esquisse 9 : Weabag 2 - Prototype Laser 2**

( Les prototypes que j'ai réalisé sont plus petits que prévu et coupés en deux morceaux car la grande découpeuse laser était en panne et j'ai du utilisé la petite découpeuse laser) 

|![esquisse9](images/protofinal1.jpg)| ![esquisse9](images/protofinal2.jpg)|
|---|---|

|![esquisse9](images/protofinal3.jpg)| ![esquisse9](images/protofinal4.jpg)|
|---|---|


|![esquisse9](images/protofinal5.jpg)| ![esquisse9](images/protofinal6.jpg)|
|---|---|


